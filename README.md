# Nginx Proxy Overview App 1.0

This is a small Node.js app which watches the nginx config file (usually `default.conf`), extracts all services from it and serves an overview page with links to all healthy services.

Also it updates the `/etc/hosts` if a new service is available and I made a volume mount. - This is neccessary for local / development containers, so you can connect to them via their domain name without having to care about anything.

## Getting Started

It should run on your host system with `npm start` but since this is part of my [Nginx Proxy Container](https://bitbucket.org/mmprivat/nginx-proxy/) I recommend using this container to run the app.

### Configuring

You can configure the app via following environment variables:

`NGINX_PROXY_FILE` - the full path to the nginx proxy config file where all services are included
`HOSTS_FILE` - the full path to the hosts file which the app will update
`HTML_TEMPLATE_FILE` - the full path to the handlebars html template file - look into the [default file](./htmlBody.handlebars) for a template how the variables will be read

### Debugging / developing

You can start the watching service (file changes will automatically trigger a reload of the app) with starting the app via

	# npm run dev

in the previously mentioned container there is also support for this.

## About the project

### Versioning

We use MAIN.MINOR number for versioning where as MAIN usually means big / breaking changes and MINOR usually means small / non breaking changes. For the version number, see at the top header or in the [tags on this repository](https://bitbucket.org/mmprivat/php-dev/downloads/?tab=tags).

### Author(s)

* **Manuel Manhart** - *Initial work*

### License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Contributing

If you want to contribute, contact the author.

### Changelog

__1.0__

* Initial app, shows an overview page of the proxies defined in the nginx file, updates `/etc/hosts` file

### Open issues

* Update the `/etc/hosts` if a new service is available & describe how it works - should be via volume mount

## Sources

* [Use npm start to launch a node app](https://coderwall.com/p/lwfndg/use-npm-start-to-launch-node-app)