class Proxies {
	constructor() {
		this.proxies = [];
	}

	startReload() {
		this.proxies.forEach(element => {
			element.healthy = false;
		});
	}

	set(proxy) {
		//console.log('Setting proxy ', proxy);
		if (this.proxies.length === 0) {
			this.proxies.push(proxy);
		} else {
			var found = false;
			this.proxies.forEach(function(element, index, theArray) {
				//console.log('Checking if proxy ' + proxy.name + ' already exists: ' + element.name);
				if (element.name === proxy.name) {
					//console.log('found ' + element.name + ' -> replacing with ' + proxy.name);
					theArray[index] = proxy;
					found = true;
				}
			});
			if (!found) {
				//console.log('did not find ' + proxy.name + ', adding now');
				this.proxies.push(proxy);
			}
		}
	}

	getAll() {
		return this.proxies;
	}
}

module.exports = Proxies