/*
const fs = require('fs');
const readline = require('readline');
*/

// domain
const ProxyConfig = require('./proxyConfig');
const Proxies = require('./proxies');

// file watcher for nginx config file
const FileWatcher = require('./fileWatcher');

// http server for serving the status as web page
const HttpServer = require('./httpServer');

// main script
const proxies = new Proxies();

const fileWatcher = new FileWatcher(proxies);
fileWatcher.readConfFile();
fileWatcher.watchConfFile();

const server = new HttpServer(proxies);
server.createServer();
