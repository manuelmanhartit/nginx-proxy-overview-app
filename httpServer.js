const http = require('http');
const fs = require('fs');
const handlebars = require('handlebars');
//const Proxies = require('./proxies');

// this can be overridden via environment variables
const DEFAULT_HTML_TEMPLATE_FILE = process.cwd() + '/' + 'htmlBody.handlebars';
var HTML_TEMPLATE_FILE=DEFAULT_HTML_TEMPLATE_FILE;

class HttpServer {
	constructor(proxies) {
		if (!! process.env.HTML_TEMPLATE_FILE) {
			HTML_TEMPLATE_FILE = process.env.HTML_TEMPLATE_FILE;
		}
		this.proxies = proxies;
		let tplContent = fs.readFileSync(HTML_TEMPLATE_FILE).toString();
		//console.log("template file content: ", tplContent);
		this.template = handlebars.compile(tplContent);
	}

	createServer() {
		var tpl = this.template;
		var proxies = this.proxies;
		http.createServer(function (request, response) {
			var context = {
				"proxies": proxies.getAll()
			};
			// Send the HTTP header 
			// HTTP Status: 200 : OK
			// Content Type: text/plain
			response.writeHead(200, { 'Content-Type': 'text/html' });
			// Send the response body as 'Hello World'
			//console.log("tpl: ", tpl);
			//console.log("proxies: ", proxies);
			var html = tpl(context);
			//console.log(html);
			response.end(html);
		}).listen(8081);

		// Console will print the message
		console.info('Server running at http://127.0.0.1:8081/');
	}
}

module.exports = HttpServer